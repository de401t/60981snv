<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <?php if (! $ionAuth->loggedIn()): ?>
        <div class="jumbotron text-center">
            <a class="navbar-brand fa fa-shopping-basket fa-3x" style="color:black" href="<?= base_url()?>"></a>
            <h1 class="display-4">NeYandex Market</h1>
            <p class="lead">Это приложение поможет потратить ваши деньги.</p>
            <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
        </div>
    <?php endif ?>
    


<?= $this->endSection() ?>
