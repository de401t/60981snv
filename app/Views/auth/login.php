<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12 ">
                <h1>Авторизация</h1>
                <p>Введите ваш логин и пароль</p>

                <?php if (isset($message)): ?>
                    <div class="alert alert-danger text-center">Ошибка авторизации</div>
                <?php endif ?>

                <?php echo form_open('auth/login');?>
                <div class="mb-3">
                    Логин:
                    <?php echo form_input($identity, '','class="form-control" value="Mark" required');?>
                </div>
                <div class="mb-3">
                    Пароль:
                    <?php echo form_input($password, '','class="form-control" value="Mark" required');?>
                </div>
                <span class="mb-3">
                    Запомнить меня
                    <?php echo form_checkbox('remember', '1', false, 'id="remember"');?>
                </span>
                <div class="mb-3 row justify-content-center">
                    <?php echo form_submit('submit', lang('Войти'), 'class="btn btn-primary"');?>
                </div>
                <?php echo form_close();?>
            </div>
        </div>

        <div class="text-center"><a class="text-decoration-none text-dark" href="forgot_password"><?php echo lang('Забыли пароль');?></a></div>
        <div class="text-center"><a class="text-decoration-none text-dark" href="register_user"><?php echo lang('Зарегестрироваться');?></a></div>
    
    </div>
    
<?= $this->endSection() ?>