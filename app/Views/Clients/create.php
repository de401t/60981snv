<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px; margin-top:150px;">

        <div class="row">
        
        <div class="col-12 d-flex justify-content-center">

            <div class="card" style="width: 28rem;">
            <img src="/new-user.svg"  class="mt-3 card-img-top" height="100px" width="100px" alt="...">
            <div class="card-body text-center">
                    <?= form_open_multipart('Clients/store'); ?>
                <div class="form-group">
                    <label for="name">ФИО</label>
                    <input type="text" class="form-control <?= ($validation->hasError('ФИО')) ? 'is-invalid' : ''; ?>" name="ФИО"
                        value="<?= old('ФИО'); ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('ФИО') ?>
                    </div>

                </div>
                <div class="form-group">
                    <label for="name">Местонахождение</label>
                    <input type="text" class="form-control <?= ($validation->hasError('Физический_адресс')) ? 'is-invalid' : ''; ?>" name="Физический_адресс"
                        value="<?= old('Физический_адресс'); ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('Физический_адресс') ?>
                    </div>

                </div>
                <div class="form-group">
                    <label for="birthday">Эл. почта</label>
                    <input type="email" class="form-control <?= ($validation->hasError('email')) ? 'is-invalid' : ''; ?>" name="email" value="<?= old('email'); ?>">
                    <div class="invalid-feedback">
                        <?= $validation->getError('birthday') ?>
                    </div>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-secondary" name="submit">Добавить клиента</button>
                </div>
                </form> 
            </div>
            </div>
        
        

        </div>
        
        </div>

        


    </div>
<?= $this->endSection() ?>