<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('Clients/update'); ?>
        <input type="hidden" name="id" value="<?= $rating["id"] ?>">

        <div class="form-group">
            <label for="name">ФИО</label>
            <input type="text" class="form-control <?= ($validation->hasError('ФИО')) ? 'is-invalid' : ''; ?>" name="ФИО"
                   value="<?= $rating["ФИО"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('ФИО') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="name">Физический адресс</label>
            <input type="text" class="form-control <?= ($validation->hasError('Физический_адресс')) ? 'is-invalid' : ''; ?>" name="Физический_адресс"
                   value="<?= $rating["Физический_адресс"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Физический_адресс') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="birthday">Email</label>
            <input type="email" class="form-control <?= ($validation->hasError('email')) ? 'is-invalid' : ''; ?>" name="email" value="<?= $rating["email"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('email') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>