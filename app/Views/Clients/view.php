<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <div class="m-3 m-md-5 m-lg-5 text-center">
            <h2>Профиль клиента</h2>
        </div>
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
            <?php use CodeIgniter\I18n\Time; ?>
            <?php if (!empty($rating)) : ?>
                <div class="card mb-3">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if ($rating['email'] == 0) : ?>
                                <img height="150" src="/man.svg" class="card-img" alt="<?= esc($rating['ФИО']); ?>">
                            <?php else:?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" class="card-img" alt="<?= esc($rating['ФИО']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($rating['ФИО']); ?></h5>
                                <p class="card-text"><?= esc($rating['ФизическийАдресс']); ?></p>
                                <p class="card-text"><?= esc($rating['email']); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                
            <?php else : ?>
                <p>Рейтинг не найден.</p>
            <?php endif ?>
        </div>
        </div>
        
        <div class="row d-flex justify-content-center">
            <div class="col-12 ">

                <div class="text-center mb-4">
                    <h2>Заказанные товары</h2>
                </div>
                <div class="list-group">
                <?php foreach ($order as $o): ?> 
                    <?php foreach ($item_order as $i_o): ?>
                        <?php if ($i_o['id_заказа'] == $o['id']) : ?>
                            <a class="list-group-item list-group-item-action ">
                                <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">ID Товара "<?= esc($i_o['id_товара']); ?>"</h5>
                                <h5 class="mb-1">
                                <?php foreach ($items as $i): ?>
                                    <?php if ($i['id'] == $i_o['id_товара']) : ?>
                                        <?= esc($i['НаименованиеТовара']); ?>
                                    <?php endif ?>
                                <?php endforeach; ?>
                                </h5>     
                                <small>Колличество <?= esc($i_o['Колличество']); ?></small>
                                </div>
                                <p class="mb-1">ID Заказа "<?= esc($i_o['id_заказа']); ?>"</p>
                            </a>    
                        <?php endif ?>
                    <?php endforeach; ?>
                    <br/>
                <?php endforeach; ?>

                </div>
            </div>
        </div>
        

    </div>
        
<?= $this->endSection() ?>