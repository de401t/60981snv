<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container">
        <div class="m-3 m-md-5 m-lg-5 text-center">
            <h2>Клиенты</h2>
        </div> 

        <div class="row">
        
        <div class="col-12 text-center">
        <div class="row">
        <?php if (!empty($rating) && is_array($rating)) : ?>

            <?php foreach ($rating as $item): ?>
                
                
                <div class="col-12 col-md-4 col-lg-4 mb-2 mb-md-4 mb-lg-4">
                <div class="card p-2 d-inline-block" style="width: 22rem; height: 22rem;">
                    
                    <?php if ($item['email'] == 0) : ?>
                        <img src="/man.svg" class="card-img-top" height=200 width=200 alt="<?= esc($item['ФИО']); ?>">
                    <?php else:?>
                        <img src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg" class="card-img-top" height=200 width=200 alt="<?= esc($item['ФИО']); ?>">
                    <?php endif ?>
                    <div class="card-body text-center">
                        <h5 class="card-title"><?= esc($item['ФИО']); ?></h5>
                        <a href="<?= base_url()?>/index.php/Clients/view/<?= esc($item['id']); ?>" class="btn btn-secondary">Просмотреть</a>
                    </div>
                </div>
                
                
                
                </div>
                

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти рейтинги.</p>
        <?php endif ?>
        </div>
        </div>
        
        </div>
        
    </div>




<?= $this->endSection() ?>