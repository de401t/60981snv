<?php namespace App\Controllers;

use App\Models\ClientModel;
use App\Models\ItemOrderModel;
use App\Models\ItemModel;
use App\Models\OrderModel;
use IonAuth\Libraries\IonAuth;

class Clients extends BaseController

{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModel();
        $data ['rating'] = $model->getRating();
        $data['ionAuth'] = new IonAuth();

        
        
        echo view('Clients/view_all', $data);
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModel();
        $data ['rating'] = $model->getRating($id);

        $order_model = new OrderModel();
        $data ['order'] = $order_model->getOrder($id);

        $item_model = new ItemModel();
        $data ['items'] = $item_model->getItems();

        $item_order_model = new ItemOrderModel();
        $data ['item_order'] = $item_order_model->getItemOrder($order_model->getOrder($id));

        

        

        
    
        echo view('Clients/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('Clients/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post')
        {
            if ($this->validate([
                'ФИО' => 'required',
                'Физический_адресс'  => 'required',
                'email'  => 'required',
            ]))
            {
                $model = new ClientModel();
                
                $model->save([
                    'ФИО' => $this->request->getPost('ФИО'),
                    'Физический_адресс' => $this->request->getPost('ФизическийАдресс'),
                    'email' => $this->request->getPost('email'),
                ]);

                session()->setFlashdata('message', lang('Curating.rating_create_success'));
                return redirect()->to('/Clients')->withInput();


            }
            else{
                echo ('Some error');
            }
        }
        else
        {
            return redirect()->to('/Clients/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModel();

        helper(['form']);
        $data ['rating'] = $model->getRating($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('Clients/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form','url']);
        echo '/Clients/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'ФИО' => 'required',
                'Физический_адресс'  => 'required',
                'email'  => 'required',
            ]))
        {
            $model = new ClientModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'ФИО' => $this->request->getPost('ФИО'),
                'Физический_адресс' => $this->request->getPost('Физический_адресс'),
                'email' => $this->request->getPost('email'),
            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/Clients');
        }
        else
        {
            return redirect()->to('/Clients/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModel();
        $model->delete($id);
        return redirect()->to('/Clients');
    }
}

