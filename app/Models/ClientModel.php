<?php namespace App\Models;
use CodeIgniter\Model;

class ClientModel extends Model
{
    protected $table = 'Покупатель'; //таблица, связанная с моделью

    protected $allowedFields = ['ФИО', 'ФизическийАдресс', 'email'];

    public function getRating($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

}

