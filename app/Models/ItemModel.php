<?php namespace App\Models;
use CodeIgniter\Model;

class ItemModel extends Model
{
    protected $table = 'Товар'; //таблица, связанная с моделью

    protected $allowedFields = ['НаименованиеТовара', 'id_категории', 'Вес', 'Цена'];

    public function getItems()
    {
        return $this->findAll();
    }

}

