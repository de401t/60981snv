<?php namespace App\Models;
use CodeIgniter\Model;

class OrderModel extends Model
{
    protected $table = 'Заказ'; //таблица, связанная с моделью

    protected $allowedFields = ['id', 'id_покупателя', 'ДатаИвремяОтправки'];

    public function getOrder($id = null)
    {
        
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id_покупателя' => $id])->findAll();
    }

}